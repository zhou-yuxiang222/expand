# expand

#### 介绍
java语言进阶

继承主要解决的问题是：共性抽取

使用的关键字：extends 

子类继承所有的方法，或者可以重写父类的多个方法，
重写父类方法或者成员遍量的方法是用和父类同样的方法名称。
同时需要注意的是：

在同一个包内，子类不继承父类的private

不在同一个包内，子类只继承父类的public protected 不能继承父类的友好关键字修饰的。
![img.png](img.png)

super()的应用

使用super调用父类的构造方法

使用super操作被隐藏的成员变量和方法

![img_1.png](img_1.png)



## 抽象
抽象方法的关键字：abstract 

抽象方法：没有方法体的方法

抽象类：包含抽象方法的类

抽象方法的格式
```
修饰符 abstract 返回值类型 方法名称(参数列表);

比如
public abstract void run():
```

抽象类的格式
```
abstract class 类名称{

}

比如
public abstract class Anmail{
    public abstract void run();
}
```
注意事项：

![img_2.png](img_2.png)
![img_3.png](img_3.png)