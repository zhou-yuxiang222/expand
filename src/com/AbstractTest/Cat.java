package com.AbstractTest;

/**
 * Created by sky.
 * Date:2022/9/29.
 * Time:9:53.
 *
 * @ClassName Cat.java
 * Description:
 * Knowledge：
 */
public class Cat extends Anmail{
    // 表示重写
    @Override
    public void eat() {
        System.out.println("猫吃鱼");
    }

    @Override
    public void say() {
        System.out.println("喵喵喵");
    }
}
