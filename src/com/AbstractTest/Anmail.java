package com.AbstractTest;

/**
 * Created by sky.
 * Date:2022/9/29.
 * Time:9:47.
 *
 * @ClassName Anmail.java
 * Description:创建一个抽象类，封装了两个行为标准，创建的额类型是动物类
 * Knowledge：抽象类的使用
 */
public abstract class Anmail {
    /**
     **定义一个抽象的方法，描述动物的吃
     **@pagarm
     */
    public abstract void eat();
    /**
     **define an abstract method
     **@pagarm: 定义一个抽象方法
     */
    public abstract void say();
}
