package com.Inherit;

/**
 * Created by sky.
 * Date:2022/9/27.
 * Time:15:43.
 *
 * @ClassName Teacher.java
 * Description:
 * Knowledge：
 */
public class Teacher extends Person{
    public void eat(){
        System.out.println("重写父类方法");
    }
    public void  say(){
        System.out.println("教师讲课");
    }
    public void eat2(){
        System.out.println("调用super方法，输出的结果");
        super.eat();
    }
}
