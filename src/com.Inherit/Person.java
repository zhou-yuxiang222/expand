package com.Inherit;

/**
 * Created by sky.
 * Date:2022/9/27.
 * Time:15:42.
 *
 * @ClassName Person.java
 * Description:
 * Knowledge：
 */
public class Person {
    public String name;
    public int age;
//无参构造方法
    public Person() {
    }
//    全参构造方法
    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }
//类方法
    public void eat(){
        System.out.println("吃东西");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
