package com.Inherit;

/**
 * Created by sky.
 * Date:2022/9/27.
 * Time:15:42.
 *
 * @ClassName Main.java
 * Description:继承：共性抽取  使用的关键字：extends
 * Knowledge：
 */
public class Main {
    //这是一个main方法，是程序的入口
    public static void main(String[] args) {
//        create an object
        Person person = new Person();
        Teacher teacher = new Teacher();

        person.setName("张三");
        teacher.setName("李四");
        System.out.println(person.getName());
        person.eat();
        System.out.println(teacher.getName());

        teacher.eat();//重写后的方法  预计输出结果：重写的父类方法
        teacher.eat2();
        teacher.say();
    }
}
